package com.gitee.hengboy.mybatis.enhance.dsl.expression;

import com.gitee.hengboy.mybatis.enhance.dsl.exception.ColumnException;

/**
 * 定义了查询表的表达式类型
 * 实现表达式接口com.gitee.hengboy.mybatis.enhance.dsl.serach.expression.Expression全部方法
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public abstract class TableExpression<T>
        extends AbstractExpression<T> {

    /**
     * 构造函数初始化根描述
     *
     * @param root 表名 或者 列名
     */
    public TableExpression(String root) {
        super(root);
    }

    /**
     * 获取定义的所有列表达式
     *
     * @return 列表达式数组
     */
    public abstract ColumnExpression[] getColumns();

    /**
     * @return 列表达式数组
     * @throws ColumnException 列异常
     */
    @Override
    public ColumnExpression<T> getSlfe() throws ColumnException {
        return null;
    }

    /**
     * 设置表的别名
     *
     * @param asName 别名内容
     * @return 表达式
     */
    @Override
    public TableExpression<T> as(String asName) {
        this.asName = asName;
        return this;
    }
}
