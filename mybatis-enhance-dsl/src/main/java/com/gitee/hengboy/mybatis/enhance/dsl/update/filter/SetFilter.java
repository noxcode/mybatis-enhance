package com.gitee.hengboy.mybatis.enhance.dsl.update.filter;

import com.gitee.hengboy.mybatis.enhance.dsl.expression.ColumnExpression;
import com.gitee.hengboy.mybatis.enhance.exception.EnhanceFrameworkException;
import lombok.Data;

/**
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@Data
public class SetFilter {
    /**
     * 更新列
     */
    private ColumnExpression columnExpression;
    /**
     * 更新值
     */
    private Object value;

    /**
     * 构造函数初始化
     *
     * @param columnExpression 更新列
     * @param value            更新值
     */
    public SetFilter(ColumnExpression columnExpression, Object value) {
        this.columnExpression = columnExpression;
        this.value = value;
    }

    /**
     * 创建一个setFilter对象实例
     *
     * @param columnExpression 更新列
     * @param value            更新值
     * @return 更新过滤对象
     * @throws EnhanceFrameworkException 框架异常
     */
    public static SetFilter set(ColumnExpression columnExpression, Object value) throws EnhanceFrameworkException {
        return new SetFilter(columnExpression, value);
    }
}
