package com.gitee.hengboy.mybatis.enhance.dsl.where.sql.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 查询条件sql实体
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@Data
public class ColumnWhereSQLEntity {
    /**
     * 参数名称列表，数组为了in、not in、<>集合使用
     */
    private List<String> paramNames = new ArrayList();
    /**
     * 生成后的sql
     */
    private List<String> sqls = new ArrayList();
    /**
     * 参数对应值列表，数组作用同paramNames集合
     */
    private List<Object> values = new ArrayList();
}
