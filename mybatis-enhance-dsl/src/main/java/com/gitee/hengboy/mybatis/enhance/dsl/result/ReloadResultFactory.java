package com.gitee.hengboy.mybatis.enhance.dsl.result;

import com.gitee.hengboy.mybatis.enhance.dsl.result.support.BasicReloadResult;
import com.gitee.hengboy.mybatis.enhance.dsl.result.support.EntityReloadResult;

/**
 * 数据返回重载工厂类
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/9
 * Time：5:28 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public class ReloadResultFactory {

    private ReloadResultFactory() { }

    /**
     * 获取数据重载返回接口实例
     * 根据传递参数自动获取对应的返回值
     *
     * @param parameterResultType 请求参数携带的返回数据类型
     * @return 重载返回值实例
     */
    public static ReloadResult newInstance(Class<?> parameterResultType) {
        // 是否为基本数据类型
        boolean isBasic = ResultReloadUtils.isBasicResultType(parameterResultType);
        /*
         * 为基本数据类型时重载基本数据类型返回值
         * 为实体数据类型时重载实体数据类型返回值
         */
        return isBasic ? new BasicReloadResult(parameterResultType) : new EntityReloadResult(parameterResultType);
    }
}
