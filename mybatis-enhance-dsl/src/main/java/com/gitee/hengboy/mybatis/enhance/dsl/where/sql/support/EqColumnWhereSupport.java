package com.gitee.hengboy.mybatis.enhance.dsl.where.sql.support;

import com.gitee.hengboy.mybatis.enhance.dsl.expression.ColumnExpression;
import com.gitee.hengboy.mybatis.enhance.dsl.where.sql.entity.ColumnWhereSQLEntity;
import com.gitee.hengboy.mybatis.enhance.exception.EnhanceFrameworkException;

/**
 * 查询条件 "="指定列sql生成
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
class EqColumnWhereSupport
        extends AbstractColumnWhereSupport {
    /**
     * 返回查询条件实体
     *
     * @param expression 表达式
     * @param index            索引位置
     * @return 查询条件实体
     * @throws EnhanceFrameworkException 框架异常
     */
    @Override
    public ColumnWhereSQLEntity getColumnWhere(ColumnExpression expression, int index) throws EnhanceFrameworkException {
        /**
         * 查询条件的值
         * 该值根据placeholer而定
         */
        Object value = expression.getValues().get(index);
        /**
         * 集合类型
         */
        if (value instanceof Object[]) {
            collectionEqValue(expression, index, (Object[]) value);
        }
        //单值
        else {
            singleEqValue(expression, index, value);
        }
        return whereSQLEntity;
    }
}
