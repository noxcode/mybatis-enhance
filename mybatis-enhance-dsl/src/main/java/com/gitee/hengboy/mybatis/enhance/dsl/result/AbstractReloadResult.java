package com.gitee.hengboy.mybatis.enhance.dsl.result;

/**
 * 重载结果集抽象类
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/9
 * Time：5:47 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public abstract class AbstractReloadResult implements ReloadResult {
    /**
     * 请求参数携带的返回类型
     */
    protected Class<?> parameterResultType;

    public AbstractReloadResult(Class<?> parameterResultType) {
        this.parameterResultType = parameterResultType;
    }
}
